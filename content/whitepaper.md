---
title: Special Relativity and Light Distance
subtitle: "A reformation of the Lorentz factor."
comments: false
date: 2018-08-18
---

## Background

Albert Einstein's' Special Theory of Relativity (<abbr title="Special Theory of Relativity">STR</abbr>)[^1] introduced concepts of light speed constancy, relativity, the Lorentz transformation, and more.  The Lorentz factor, in particular, has proven to be a reliable framework over decades of experimentation.   Acceptance of the emperical validity of the Lorentz factor is largely universal.  

Nevertheless, the complete integration of (<abbr title="Special Theory of Relativity">STR</abbr>) remains an unresolved problem in physics.  Although the Hafele–Keating experiment[^2], GPS, and more lend validity these effects, (<abbr title="Special Theory of Relativity">STR</abbr>) denyers tend to use the complications, along with a selective rejection of emperical evidence, to produce any number of invalid theories.  

We find a resolution of these issues if we assume that the Lorentz factor is derivable and reconcilable through classical mechanics.  Although this assumption may appear irreconcilable at first sight, the following will show our assumption to affirm light speed constancy, the Lorentz factor, and the overwhelming body of evidence produced since Einstein's groundbreaking work.  

## Constancy

In the following, let us have a single stationary coordinate system in which the Newtonian equations hold.  If a material point be at rest in this system, then its position in this system can be found out by a measuring rod, and can be expressed by the methods of Euclidean Geometry, or in Cartesian coordinates.  We have the system of coordinates Einstein described in §1 and §2 of his work, and not of the kind he described in §3.  Let us also accept light speed constancy: 

>  In the following we [...] introduce the further assumption, —an assumption which is at the first sight quite irreconcilable with the former one— that light is propagated in vacant space, with a velocity $$c$$ which is independent of the nature of motion of the emitting body.[^1]

> Every ray of light moves in the "stationary coordinate system" with the same velocity c, the velocity being independent of the condition whether this ray of light is emitted by a body at rest or in motion.[^1]

Let us have a rigid rod with ends $$A$$ and $$B$$ and with its axis laid along the X-axis of the stationary coordinate system.  Let the rod have a length of $$ l \_{rod} $$ and let it have a constant velocity of $$ v \_{rod} $$ along the X-axis in the direction of $$B$$ from $$A$$.  At the time $$ t_0 $$, a ray of light goes out $$A$$, reflected at $$B$$ at time $$ t_1 $$, and arrives back at $$A$$ at time $$ t_2 $$.  We have the timing of events, in agreement with <abbr title="Special Theory of Relativity">STR</abbr>, as follows:

$$ t_1 - t_0 = \frac{l \_{rod}}{c - v \_{rod} } $$ and $$ t_2 - t_1 = \frac{l \_{rod}}{c + v \_{rod} } $$

Refactored:

$$ t_2-t_0 = (t_1-t_0) + (t_2 - t_1) = \frac{l \_{rod}}{c - v \_{rod} } + \frac{l \_{rod}}{c + v \_{rod} } $$

We may also determine distances given fixed points.  When light is emitted from $$A$$ at time $$t_0$$, affix a stationary point $$P_0$$ at the axes where the light is emitted.  When the light reaches $$B$$ at time $$t_1$$, record the reflection point $$P_1$$.  Once the reflected light returns to $$A$$ at time $$t_2$$, we record the meeting point $$P_2$$.  

Let $$ d \_{0\rightarrow1}$$ be the distance between $$P_0$$ and $$P_1$$ and let $$d \_{2\leftarrow1}$$ be the distance between $$P_1$$ and $$P_2$$.  If we consider the rod's movement away from $$P_0$$ until the moment we establish $$P_1$$, we see that $$ d \_{0 \rightarrow 1}$$ is greater than $$l \_{rod}$$.  Similarly, if we consider the rod's movement passing $$P_1$$ until the moment we establish $$P_2$$, we see that $$d \_{2\leftarrow1}$$ is less than $$l \_{rod}$$.  $$ d \_{0 \rightarrow 1}$$ and $$ d \_{2\leftarrow1}$$ and $$l \_{rod}$$ are all equal only when given a rod with zero velocity.  We have the following equations:

<details><summary>Expand full $$d$$ refactoring sequence</summary>

$$ \Delta t = \frac{\Delta  x}{ v \_{light} } = \frac{d \_{0\rightarrow1}}{c} = t_1 - t_0 = \frac{l \_{rod}}{c - v \_{rod} }$$, refactored: 

$$ d \_{0\rightarrow1} = l \_{rod} \left( \frac{c}{c - v \_{rod} } \right) $$

and

$$ \Delta t = \frac{\Delta  x}{ v \_{light} } = \frac{d \_{2\leftarrow1}}{c} = t_2 - t_1 = \frac{l \_{rod}}{c + v \_{rod} }$$, refactored: 

$$ d \_{2\leftarrow1} = l \_{rod} \left( \frac{c}{c + v \_{rod} } \right) $$

Therefore:

</details>

$$ d \_{0\rightarrow1} = l \_{rod} \left( \frac{c}{c - v \_{rod} } \right) $$ and $$ d \_{2\leftarrow1} = l \_{rod} \left( \frac{c}{c + v \_{rod} } \right) $$

## Transformation

Let us determine the applicability of the Lorentz factor according to the stationary system we established earlier.  As $$v \_{rod}$$ increases, $$ d \_{0 \rightarrow 1}$$ increases and $$d \_{2\leftarrow1}$$ decreases.  We record the round trip time for light to travel from $$P_0$$, until its reflection at $$P_1$$ and subsequent arrival at $$P_2$$ as $$T$$.  We distinguish timing of a stationary rod and a moving rod using $$T_S$$ and $$T_M$$ respectively.  The ratio $$\Phi$$ between $$T_S$$ and $$T_M$$ is determined as follows:

$$ \Phi=\frac{T_M}{T_S}$$

<details><summary>Expand $$ \Phi $$ full refactoring sequence</summary>

$$ \Phi(T_S)=T_M$$

$$ \Phi\left(\frac{l \_{rod}}{c - 0 } + \frac{l \_{rod}}{c + 0}\right) = \frac{l \_{rod}}{c - v \_{rod} } + \frac{l \_{rod}}{c + v \_{rod} }$$

$$ \Phi\left(\frac{2 l \_{rod}}{c} \right) = \frac{cl \_{rod} - l \_{rod}v \_{rod} + cl \_{rod} + l \_{rod}v \_{rod} }{c^2 + cv \_{rod} - cv \_{rod} - v \_{rod}^2 } $$

$$ \Phi\left(\frac{2 l \_{rod}}{c} \right) = \frac{2cl \_{rod} }{c^2 - v \_{rod}^2 }$$

$$ \Phi\left(\frac{1}{c} \right) = \frac{c}{c^2 - v \_{rod}^2 }$$

$$ \Phi = \frac{c^2}{c^2 - v \_{rod}^2} $$

This ratio is equally derivable from time or distance.  Let $$D$$ be the distance light travels for $$T$$.  Let $$D$$ given stationary rod and a moving rod be $$D_S$$ and $$D_M$$ respectively.

$$ D_M = l \_{rod} \left( \frac{c}{c - v \_{rod} } \right) + l \_{rod} \left( \frac{c}{c + v \_{rod} } \right)$$

$$ D_S = l \_{rod} \left( \frac{c}{c - 0  } \right) + l \_{rod} \left( \frac{c}{c + 0  } \right) = l \_{rod} + l \_{rod} = 2 l \_{rod}$$

$$ \Phi = \frac{T_M}{T_S} = \frac{D_M}{D_S}$$

</details>

$$ \Phi = \frac{c^2}{c^2 - v \_{rod}^2} $$

Suppose we initiate a series of light pulses, with half originating at $$A$$, and half at $$B$$.  Light's round trip travel time $$T$$ is balanced by light's travel in both directions, therefore $$T_S$$ and $$T_M$$ are unaffected by the rotation of origins.  However, obtaining the precise timing of events in a single direction, requires knowing the orientation of the rod relative to the pulse.  If we do not have this information, we may still obtain an approximate ratio that will hold true over time.  For example, we can compare the average progression light across the rod from $$P_0$$ to $$P_1$$, where average distance the light is traveling when given a moving and stationary rod is $$d_m$$ and $$d_s$$ respectively.  Similarly we can compare the average time light is traveling with $$t_m$$ and $$t_s$$ respectively.  We have the following:

$$\tilde{\gamma} = \sqrt{ \frac{1}{\Phi}} $$

<details><summary>Expand $$\tilde{\gamma}$$ full refactoring sequence</summary>

$$\tilde{\gamma} = \sqrt{ \frac{1}{ \frac{c^2}{c^2 - v \_{rod}^2} } } $$

$$\tilde{\gamma} = \sqrt{ \frac{c^2 - v \_{rod}^2}{c^2} } $$

</details>

$$\tilde{\gamma} = \sqrt{ 1- \frac{v \_{rod}^2}{c^2} } $$

We therefore note $$\tilde{\gamma}$$ is congruent with the Lorentz factor and also classical mechanics.

## Testing

Consider the Hafele–Keating experiment[^2] where atomic clocks were flown around the globe and their time variations were measured upon their return.  The recorded time differences were determined to be consistent with <abbr title="Special Theory of Relativity">STR</abbr> and used as confirmation of “time dilation.”  

However, note that atomic clocks utilize light in their measurement of atom activity.  Generally, light is emitted and the atom activity is observed by a receptor.  The orientation of the atomic clock's light emitter and receptor, along with the orientation of the clock during its flight, the orientation of the aircraft as it travels, the rotation of the earth, the orbit of the earth, and other rotational factors would all be required obtain the precise frequency the clock must use at any particular moment.  Instead, the experiment substituted the $$ \tilde{\gamma} $$ Lorentz approximation to yield an approximation for the duration of the flight.

Similarly, if we consider the atomic clocks aboard GPS satellites, determining the precise timing would require resolving the instantaneous vectors, the changing orientation of the satellites, their orbit of the earth, the earth's orbit of the sun, and the like.  In effect, we have obtained an approximation by using the $$ \tilde{\gamma} $$ Lorentz transformation to set their clock frequencies.  However, if we determine the precise positioning and rotation of the equipment, we could work towards instantaneous frequency adjustments so that atomic "clock drift" is diminished or eliminated between the satellites.

## Relativity

Consider Einstein's definition of synchronism in §1 of his work:

> Let us have a co-ordinate system, in which the Newtonian equations hold.

> If a material point be at rest in this system, then its position in this system can be found out by a measuring rod, and can be expressed by the methods of Euclidean Geometry, or in Cartesian co-ordinates.

> [synchronism is] the time which light requires in traveling from $$A$$ to $$B$$ is equivalent to the time which light requires in traveling from $$B$$ to $$A$$[^1]

Einstein's definition of synchronism is constructed and validated on the condition that points $$A$$ and $$B$$ are stationary.  Given the constancy of the speed of light, the time for light to travel between fixed points $$A$$ and $$B$$ will be equal.  However, in §2, he imparts movement to $$A$$ and $$B$$:

> Let us suppose that the two clocks synchronous with the clocks in the system at rest are brought to the ends A, and B of a rod, i.e., the time of the clocks correspond to the time of the stationary system at the points where they happen to arrive ; these clocks are therefore synchronous in the stationary system.  We further imagine that there are two observers at the two watches, and moving with them[^1]

The criterion Einstein proposes to determine the existence or absence of synchronism in §2 requires fixed points, yet Einstein's test does not actually implement the fixed points.  Einstein therefore fails to adhere to his own criterion in his rejection of synchronism.  

We may still assess the presence or absence of synchronism according to Einstein's criterion using fixed points ($$P_0$$, $$P_1$$, and $$P_2$$).  When considering fixed points, we have reason to affirm the existence of synchronism:

$$ \frac{l \_{rod}}{c - v \_{rod} } = \frac{l \_{rod}}{c + v \_{rod} } $$ is true when $$v \_{rod} =0m/s$$

We have no reason to reject synchronism.  Rather, we have reason to affirm synchronism.  Let us also consider Einstein's axiomatic rejection of absolute rest:

> no properties of observed facts correspond to a concept of absolute rest[^1]

However, Einstein failed to assert a precise definition of absolute rest.  Let us assume a definition of absolute rest according to the single stationary coordinate system we established earlier:

<dl>
  <dt>Absolute Rest</dt>
  <dd>the additive inverse of light's speed in a vacuum ($$c-c$$) for all axes</dd>
</dl>

We may also take this opportunity to clarify Einstein's definition of synchronism.  Similarly, to avoid ambiguity, we may also clarify the definition of time as used in the International System of Units:

<dl>
  <dt>Synchronism</dt>
  <dd>the time which light in a vacuum requires in traveling from $$A$$ to $$B$$ is equivalent to its time from $$B$$ to $$A$$, where $$A$$ and $$B$$ are at absolute rest.</dd>
  <dt>Unit of time (second)</dt>
  <dd><abbr title="To Be Determined">TBD</abbr> periods of the radiation corresponding to the transition between the two hyperfine levels of the ground state of the cesium 133 atom observed at absolute rest (the speed of light minus the speed of light for all axes)</dd>
</dl>

## Conclusion

The Lorentz approximation is compatible with and affirmed by a single stationary system of coordinates according to classical mechanics.  Light is propagated in vacant space, with a velocity $$c$$ which is independent of the nature of motion of the emitting body.  Absolute rest is affirmed as the additive inverse of light's speed in a vacuum ($$c-c$$) for all axes.  Synchronism is affirmed as the time which light in a vacuum requires in traveling from $$A$$ to $$B$$ is equivalent to its time from $$B$$ to $$A$$, where $$A$$ and $$B$$ are at absolute rest.  Special relativity does not affirm supposed consequences, such as length contraction, time dilation, and the like.

Einstein is well known to hold truth as an ideal; he anticipated rejection of his, or any other theory, once a logical error was discovered.  Einstein’s work rightfully remains among the great human achievements.  We should remain immensely grateful for the benefits we were all afforded due Einstein's outstanding contribution.

[^1]: <cite>Einstein, A. [On the Electrodynamics of Moving Bodies (Annalen der Physik, 1905)](https://en.wikisource.org/wiki/On_the_Electrodynamics_of_Moving_Bodies). Translation by Megh Nad Saha in The Principle of Relativity: Original Papers by A. Einstein and H. Minkowski, University of Calcutta, 1920, pp. 1–34:</cite>
[^2]: <cite>Hafele, J. & Keating, R.  ["Around-the-World Atomic Clocks: Predicted Relativistic Time Gains"](http://personal.psu.edu/rq9/HOW/Atomic_Clocks_Predictions.pdf) (Science, New Series Vol. 177, No. 4044, 1972).  Pages 166-168.</cite>
